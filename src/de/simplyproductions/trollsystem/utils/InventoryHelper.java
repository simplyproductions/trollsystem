package de.simplyproductions.trollsystem.utils;

import java.util.List;

public class InventoryHelper {

    /**
     * This method is used when you have for example a list of all online players
     * and you want to fill an inventory with items based on the players, but the
     * inventory is too small. With this method you're able to split the original
     * list into pieces for filling the inventory.
     *
     * @param original     the list you want the sublist from.
     * @param itemsPerPage how many items you want per page.
     * @param page         the page of inventory you need the items for.
     * @return the sublist for the given page.
     */
    public static List<?> getPageContent(List<?> original, Integer itemsPerPage, Integer page) {
        List<?> list;

        if (original.size() <= itemsPerPage) {
            list = original;
        } else {
            list = original.subList(((page - 1) * (9 * 3)), ((original.size() > page * (9 * 3) ? (page * (9 * 3)) : original.size())));
        }

        return list;
    }

}
