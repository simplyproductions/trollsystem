package de.simplyproductions.trollsystem.utils;

import de.chatvergehen.spigotapi.util.other.Debugger;
import de.simplyproductions.trollsystem.main.Main;
import de.simplyproductions.trollsystem.utils.modules.onetime.*;
import de.simplyproductions.trollsystem.utils.modules.toggleable.TrollModule_Freeze;
import de.simplyproductions.trollsystem.utils.modules.toggleable.TrollModule_Sandwalk;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public class TrollModuleManager {

    private static Debugger debugger = new Debugger(TrollModuleManager.class);
    private static List<TrollModule> modules = new ArrayList<>();

    public static void registerModules() {
        // TOGGLEABLE
        addModule(new TrollModule_Freeze());

        // ONE-TIME
        addModule(new TrollModule_Creeper());
        addModule(new TrollModule_DOOM());
        addModule(new TrollModule_MLG());
        addModule(new TrollModule_MessyInventory());
        addModule(new TrollModule_Sandwalk());

        debugger.sendDebugMessage("TrollSystem > Loaded " + modules.size() + " Module" + (modules.size() != 1 ? "s" : "") + ".", Debugger.SendMethod.CONSOLE);
    }

    private static void addModule(TrollModule module) {
        Bukkit.getPluginManager().registerEvents(module, Main.getInstance());
        modules.add(module);

        // ONLY IF NOT IN PERFORMANCE MODE
        if (ServerMode.valueOf(Config.Configuration.get(Config.Configuration.SERVERMODE).toString().toUpperCase()) != ServerMode.PERFORMANCE) {
            if (module instanceof Runnable) {
                ((Runnable) module).run();
            }
        }
    }

    public static TrollModule getByName(String name) {
        for (TrollModule module : modules) {
            if (module.getName().equalsIgnoreCase(ChatColor.stripColor(name))) {
                return module;
            }
        }
        return null;
    }

    public static List<TrollModule> getModules() {
        return modules;
    }

    public static List<TrollModule> getToggleableModules() {
        List<TrollModule> modules = new ArrayList<>();

        for (TrollModule module : getModules()) {
            if(modules instanceof ToggleableTrollModule) {
                modules.add(module);
            }
        }
        return modules;
    }

    public static List<TrollModule> getOnetimeModules() {
        List<TrollModule> modules = new ArrayList<>();

        for (TrollModule module : getModules()) {
            if(modules instanceof OneTimeTrollModule) {
                modules.add(module);
            }
        }
        return modules;
    }

    public static String getPermission(TrollModule module) {
        return "Troll.Module." + module.getName().replace(" ", "_");
    }

}
