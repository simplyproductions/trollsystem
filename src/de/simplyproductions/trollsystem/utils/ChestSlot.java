package de.simplyproductions.trollsystem.utils;

public enum ChestSlot {
    EINS(new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8}),
    ZWEI(new Integer[]{9, 10, 11, 12, 13, 14, 15, 16, 17}),
    DREI(new Integer[]{18, 19, 20, 21, 22, 23, 24, 25, 26}),
    VIER(new Integer[]{27, 28, 29, 30, 31, 32, 33, 34, 35}),
    FUENF(new Integer[]{36, 37, 38, 39, 40, 41, 42, 43, 44}),
    SECHS(new Integer[]{45, 46, 47, 48, 49, 50, 51, 52, 53});

    public Integer[] slots;

    ChestSlot(Integer[] slots) {
        this.slots = slots;
    }

    public static Integer getSlot(Integer row, Integer slot) {
        ChestSlot cs = getByNumber(row);
        return getSlot(cs, slot);
    }

    public static Integer getSlot(ChestSlot chestSlot, Integer slot) {
        return chestSlot.slots[slot - 1];
    }

    public static ChestSlot getByNumber(Integer row) {
        switch (row) {
            case 1:
                return EINS;
            case 2:
                return ZWEI;
            case 3:
                return DREI;
            case 4:
                return VIER;
            case 5:
                return FUENF;
            case 6:
                return SECHS;
            default:
                return null;
        }
    }

    public static Integer getRowByChestSlot(ChestSlot cs) {
        switch (cs) {
            case EINS:
                return 1;
            case ZWEI:
                return 2;
            case DREI:
                return 3;
            case VIER:
                return 4;
            case FUENF:
                return 5;
            case SECHS:
                return 6;
            default:
                return null;
        }
    }

}
