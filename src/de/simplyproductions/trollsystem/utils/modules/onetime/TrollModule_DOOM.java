package de.simplyproductions.trollsystem.utils.modules.onetime;

import de.simplyproductions.trollsystem.utils.OneTimeTrollModule;
import de.simplyproductions.trollsystem.utils.TrollModeManager;
import de.simplyproductions.trollsystem.utils.TrollModule;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Pig;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class TrollModule_DOOM extends OneTimeTrollModule implements TrollModule {


    @Override
    public boolean trigger(Player executor) {
        OfflinePlayer off_target = TrollModeManager.getTrolling(executor);
        if (off_target.isOnline()) {

            Player target = Bukkit.getPlayer(off_target.getUniqueId());
            Location location = target.getLocation();
            PigZombie pig = target.getWorld().spawn(location, PigZombie.class);
            pig.setAngry(true);
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return "START_DOOM_MODE";
    }

    @Override
    public List<String> getDescription() {
        return Arrays.asList("RIP AND TEAR UNTIL ITS DONE");
    }

    @Override
    public Material getIcon() {
        return Material.GOLDEN_SWORD;
    }
}
