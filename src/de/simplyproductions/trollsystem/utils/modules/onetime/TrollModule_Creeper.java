package de.simplyproductions.trollsystem.utils.modules.onetime;

import de.simplyproductions.trollsystem.main.Main;
import de.simplyproductions.trollsystem.utils.OneTimeTrollModule;
import de.simplyproductions.trollsystem.utils.TrollModeManager;
import de.simplyproductions.trollsystem.utils.TrollModule;
import org.bukkit.*;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.Arrays;
import java.util.List;

public class TrollModule_Creeper extends OneTimeTrollModule implements TrollModule {

    @Override
    public String getName() {
        return "Creeper";
    }

    @Override
    public List<String> getDescription() {
        return Arrays.asList(
                "Spawns creepers around the player."
        );
    }

    @Override
    public Material getIcon() {
        return Material.CREEPER_SPAWN_EGG;
    }

    @Override
    public boolean trigger(Player executor) {
        OfflinePlayer offlineTarget = TrollModeManager.getTrolling(executor);

        if (offlineTarget.isOnline()) {
            Player target = Bukkit.getPlayer(offlineTarget.getUniqueId());
            Creeper creeper = target.getWorld().spawn(target.getLocation(), Creeper.class);
            return true;
        }
        return false;
    }
}
