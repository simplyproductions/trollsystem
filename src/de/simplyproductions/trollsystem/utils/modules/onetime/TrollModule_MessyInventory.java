package de.simplyproductions.trollsystem.utils.modules.onetime;

import de.simplyproductions.trollsystem.utils.OneTimeTrollModule;
import de.simplyproductions.trollsystem.utils.TrollModeManager;
import de.simplyproductions.trollsystem.utils.TrollModule;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.*;

public class TrollModule_MessyInventory extends OneTimeTrollModule implements TrollModule {
    @Override
    public String getName() {
        return "Messy Inventory";
    }

    @Override
    public List<String> getDescription() {
        return Arrays.asList("Makes a mess of the inventory.");
    }

    @Override
    public Material getIcon() {
        return Material.CHEST;
    }

    @Override
    public boolean trigger(Player executor) {
        OfflinePlayer off_target = TrollModeManager.getTrolling(executor);
        if (off_target.isOnline()) {
            Player target = Bukkit.getPlayer(off_target.getUniqueId());

            PlayerInventory inv = target.getInventory();

            ItemStack[] items = inv.getContents();
            List<ItemStack> itemList = Arrays.asList(items);

            Collections.shuffle(itemList);
            itemList.toArray(items);

            inv.setContents(items);
            return true;
        }
        return false;
    }

}
