package de.simplyproductions.trollsystem.utils.modules.onetime;

import com.mysql.fabric.xmlrpc.base.Array;
import de.simplyproductions.trollsystem.utils.OneTimeTrollModule;
import de.simplyproductions.trollsystem.utils.TrollModeManager;
import de.simplyproductions.trollsystem.utils.TrollModule;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.List;

public class TrollModule_MLG extends OneTimeTrollModule implements TrollModule {
    @Override
    public String getName() {
        return "MLG";
    }

    @Override
    public List<String> getDescription() {
        return Arrays.asList("Haha Player go BOOM");
    }

    @Override
    public Material getIcon() {
        return Material.TNT;
    }

    @Override
    public boolean trigger(Player executor) {
        OfflinePlayer off_target = TrollModeManager.getTrolling(executor);
        if (off_target.isOnline()) {

            Player target = Bukkit.getPlayer(off_target.getUniqueId());
            Location location = target.getLocation();
            TNTPrimed tnt = target.getWorld().spawn(location, TNTPrimed.class);

            tnt.setCustomName("Boom");
            tnt.setFuseTicks(1);

            Vector vector = target.getLocation().getDirection();
            vector.setY(.75D);
            // vector.setX(0);   This will cancel the any movements.
            // vector.setZ(0);   This will cancel the any movements.
            target.setVelocity(vector);

            target.getWorld().playSound(location, Sound.ENTITY_FIREWORK_ROCKET_BLAST, 2, 1);
            return true;
        }
        return false;
    }

    @EventHandler
    public void explode(EntityExplodeEvent event) {

        Entity ent = event.getEntity();
        if (ent instanceof TNTPrimed) {

            TNTPrimed tnt = (TNTPrimed) event.getEntity();
            if (tnt.getCustomName() != null) {
                if (tnt.getCustomName().equalsIgnoreCase("Boom")) {
                    event.setYield(0);
                    event.blockList().clear();
                }
            }
        }
    }

    @EventHandler
    public void cancelDamage(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof TNTPrimed) {
            TNTPrimed tnt = (TNTPrimed) event.getDamager();

            if (tnt.getCustomName() != null) {
                if (tnt.getCustomName().equalsIgnoreCase("Boom")) {
                    event.setCancelled(true);
                }
            }
        }
    }

}
