package de.simplyproductions.trollsystem.utils.modules.toggleable;

import de.simplyproductions.trollsystem.listeners.custom.PlayerWalkEvent;
import de.simplyproductions.trollsystem.utils.ToggleableTrollModule;
import de.simplyproductions.trollsystem.utils.TrollModeManager;
import de.simplyproductions.trollsystem.utils.TrollModule;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrollModule_Sandwalk extends ToggleableTrollModule implements TrollModule {

    public static ArrayList<OfflinePlayer> active = new ArrayList<>();

    @Override
    public String getName() {
        return "Sandwalk";
    }

    @Override
    public List<String> getDescription() {
        return Arrays.asList("This will make the blocks under the", "player fall down.");
    }

    @Override
    public Material getIcon() {
        return Material.SAND;
    }

    @Override
    public List<OfflinePlayer> getActive() {
        return active;
    }

    @EventHandler
    public void sandspawn(PlayerWalkEvent event) {
        Player p = event.getPlayer();

        if (getActive().contains(p)) {
            p.getWorld().spawnFallingBlock(p.getLocation().subtract(0, 1, 0), p.getLocation().subtract(0, 1, 0).getBlock().getBlockData());
            p.getLocation().subtract(0, 1, 0).getBlock().setType(Material.AIR);
        }
    }

    @Override
    public boolean toggle(Player executor) {
        OfflinePlayer off_target = TrollModeManager.getTrolling(executor);

        if (off_target.isOnline()) {
            if (!getActive().contains(off_target)) {
                getActive().add(off_target);
            } else {
                getActive().remove(off_target);
            }
            return getActive().contains(off_target);
        }
        return false;
    }
}
