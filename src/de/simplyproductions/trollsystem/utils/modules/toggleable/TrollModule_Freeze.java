package de.simplyproductions.trollsystem.utils.modules.toggleable;

import de.simplyproductions.trollsystem.listeners.custom.PlayerWalkEvent;
import de.simplyproductions.trollsystem.utils.Item;
import de.simplyproductions.trollsystem.utils.ToggleableTrollModule;
import de.simplyproductions.trollsystem.utils.TrollModeManager;
import de.simplyproductions.trollsystem.utils.TrollModule;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrollModule_Freeze extends ToggleableTrollModule implements TrollModule {

    public static ArrayList<OfflinePlayer> active = new ArrayList<>();

    @Override
    public String getName() {
        return "Freeze";
    }

    @Override
    public List<String> getDescription() {
        return Arrays.asList(
                "If a player is frozen, he can't move."
        );
    }

    @Override
    public Material getIcon() {
        return Material.SNOWBALL;
    }

    @EventHandler
    public void move(PlayerWalkEvent e) {
        Player p = e.getPlayer();

        if (active.contains(p)) {
            e.setCancelled(true);
        }
    }

    @Override
    public List<OfflinePlayer> getActive() {
        return active;
    }

    @Override
    public boolean toggle(Player executor) {
        OfflinePlayer target = TrollModeManager.getTrolling(executor);

        if (target.isOnline()) {
            if (!getActive().contains(target)) {
                getActive().add(target);
            } else {
                getActive().remove(target);
            }
            return getActive().contains(target);
        }
        return false;
    }

}
