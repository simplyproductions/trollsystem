package de.simplyproductions.trollsystem.utils;

import org.bukkit.Material;
import org.bukkit.event.Listener;

import java.util.List;

public interface TrollModule extends Listener {

    /*---------------------------------------------------------------------------

      If a Module is toggleable you have to add "extends ToggleableTrollModule"
      in front of "implements TrollModule".

    ---------------------------------------------------------------------------*/

    String getName();

    List<String> getDescription();

    Material getIcon();

}
