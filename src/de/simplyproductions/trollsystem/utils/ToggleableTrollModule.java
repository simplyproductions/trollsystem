package de.simplyproductions.trollsystem.utils;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.List;

public abstract class ToggleableTrollModule implements TrollModule {

    public abstract List<OfflinePlayer> getActive();

    public abstract boolean toggle(Player executor);

}
