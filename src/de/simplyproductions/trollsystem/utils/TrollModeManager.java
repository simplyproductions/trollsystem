package de.simplyproductions.trollsystem.utils;

import de.chatvergehen.spigotapi.util.filemanaging.ConfigEditor;
import de.chatvergehen.spigotapi.util.filemanaging.FileBuilder;
import de.simplyproductions.trollsystem.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class TrollModeManager {

    private static ItemList items = new ItemList();

    private static FileBuilder getFile(Player player) {
        FileBuilder file = new FileBuilder(Main.getInstance().getDataFolder() + "//Data//Players//" + player.getUniqueId().toString(), "trollmode.yml");
        return file;
    }

    private static ConfigEditor getConfig(Player player) {
        return getFile(player).getConfig();
    }

    private static void setup(Player player) {
        if (!getFile(player).exists()) {
            getFile(player).create();
            getConfig(player).set("Active", false);
            getConfig(player).set("Trolling", UUID.randomUUID().toString());
        }
    }

    public static boolean getActive(Player player) {
        FileBuilder file = getFile(player);
        ConfigEditor config = getConfig(player);

        setup(player);

        return (boolean) getConfig(player).get("Active");
    }

    public static void setActive(Player player, boolean active) {
        setup(player);
        getConfig(player).set("Active", active);

        // TODO: when false: deactivate all trolls for previous target.
    }

    public static OfflinePlayer getTrolling(Player player) {
        setup(player);
        return Bukkit.getOfflinePlayer(UUID.fromString(String.valueOf(getConfig(player).get("Trolling"))));
    }

    public static void setTrolling(Player player, OfflinePlayer trolling) {
        setup(player);
        getConfig(player).set("Trolling", trolling.getUniqueId().toString());

        // TODO: deactivate all trolls for previous target.
    }

    public static void giveItems(Player player, boolean clear) {
        player.getInventory().setItem(0, items.getTroll().getItemStack());

        if (clear) {
            player.getInventory().clear();

            player.getInventory().setHelmet(new ItemStack(Material.AIR));
            player.getInventory().setChestplate(new ItemStack(Material.AIR));
            player.getInventory().setLeggings(new ItemStack(Material.AIR));
            player.getInventory().setBoots(new ItemStack(Material.AIR));
        }
    }

    public static String getTrollMenuTitle() {
        return "§bTrollMenu";
    }

    public static String getPlayerSelectionMenuTitle() {
        //return "§bTrollMenu §7- §3Player Selection";
        return "§bPlayer Selection";
    }

    public static Item getPlayerChooser(Player player) {
        Item chooser = new Item(Material.PLAYER_HEAD)
                .setName("§6Choose a player");

        chooser.setSkullOwner("MHF_Question");

        try {
            if (getTrolling(player) != null) {
                OfflinePlayer op = getTrolling(player);

                chooser.setSkullOwner(op.getName());
                chooser.addLoreLine("§7● §fTarget: §e" + op.getName());
            } else {
                chooser.addLoreLine("§7● §cYou haven't selected a target yet.");
            }
        } catch (Exception e) {
            // SHUT
        }

        chooser.setSkullOwner(player.getName());
        return chooser;
    }

    public static Item getToggleableModuleIcon() {
        return new Item(Material.LEVER)
                .setName("§aToggleable Modules")
                .addLoreLine("§7● §eSelect a toggleable module.");
    }

    public static Item getOneTimeModuleIcon() {
        return new Item(Material.STONE_BUTTON)
                .setName("§aOnetime Modules")
                .addLoreLine("§7● §eSelect an onetime module.");
    }

    public static void openTrollMenu(Player player) {
        Inventory inv = Bukkit.createInventory(null, 9 * 4, getTrollMenuTitle());
        Item placeholder = new Item(Material.BLACK_STAINED_GLASS_PANE)
                .setName("§6");

        for (int i = 0; i < inv.getSize(); i++) {
            inv.setItem(i, placeholder.getItemStack());
        }

        inv.setItem(ChestSlot.getSlot(1, 5), getPlayerChooser(player).getItemStack());

        Item information = new Item(Material.PAPER)
                .setName("§6Plugin Information")
                .addLoreLine("§8----------------")
                .addLoreLine("§fVersion: §e" + Main.getInstance().getDescription().getVersion())
                .addLoreLine("§fModules: §e" + TrollModuleManager.getModules().size())
                .addLoreLine("   §7> §fToggleable: §e" + TrollModuleManager.getToggleableModules().size())
                .addLoreLine("   §7> §fOnetime: §e" + TrollModuleManager.getOnetimeModules().size());

        if (getTrolling(player) != null) {
            inv.setItem(ChestSlot.getSlot(2, 3), getToggleableModuleIcon().getItemStack());
            inv.setItem(ChestSlot.getSlot(3, 4), getOneTimeModuleIcon().getItemStack());
        } else {
            Item needTroller = new Item(Material.BARRIER)
                    .setName("§4You haven't set a target yet")
                    .addLoreLine("§7● §cClick on the head in the top")
                    .addLoreLine("§cof this inventory for opening")
                    .addLoreLine("§cthe player selection menu.");

            inv.setItem(ChestSlot.getSlot(2, 3), needTroller.getItemStack());
            inv.setItem(ChestSlot.getSlot(3, 4), needTroller.getItemStack());
        }

        Item soon = new Item(Material.BARRIER)
                .setName("§4Not aviable yet")
                .addLoreLine("§7● §cIn the future you'll find")
                .addLoreLine("§ca new feature here.");

        inv.setItem(ChestSlot.getSlot(2, 7), soon.getItemStack());
        inv.setItem(ChestSlot.getSlot(3, 6), soon.getItemStack());

        inv.setItem(ChestSlot.getSlot(4, 9), information.getItemStack());

        player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 2, 1);
        player.openInventory(inv);
    }

    public static String getPreviousPageItemName() {
        return "§2« §aPrevious";
    }

    public static String getNextPageItemName() {
        return "§aNext §2»";
    }

    public static Item getBack() {
        return new Item(Material.ARROW)
                .setName("§4« §cFuck go back");
    }

    public static void openPlayerSelection(Player player, Integer page) {
        int invsize = 9 * 5;
        Inventory inv = Bukkit.createInventory(null, invsize, getPlayerSelectionMenuTitle());
        int itemsPerPage = invsize - (9 * 2);
        List<Player> players = new ArrayList<>();
        Bukkit.getOnlinePlayers().forEach(pl -> {
            if (pl != player) {
                players.add(pl);
            }
        });

        Item placeholder = new Item(Material.BLACK_STAINED_GLASS_PANE)
                .setName("§6");

        for (int i = ChestSlot.getSlot(4, 1); i <= ChestSlot.getSlot(4, 9); i++) {
            inv.setItem(i, placeholder.getItemStack());
        }

        boolean previous = (page > 1);
        boolean next = players.size() >= (itemsPerPage * page);

        Item previousIcon = new Item(Material.STONE)
                .setName(getPreviousPageItemName());

        if (previous) {
            previousIcon.setMaterial(Material.SUGAR);
            previousIcon.addLoreLine("§7● §eClick to change to page " + page + ".");
        } else {
            previousIcon.setMaterial(Material.GUNPOWDER);
            previousIcon.addLoreLine("§7● §eThere's no previous page.");
        }

        Item nextIcon = new Item(Material.STONE)
                .setName(getNextPageItemName());

        if (next) {
            nextIcon.setMaterial(Material.SUGAR);
            nextIcon.addLoreLine("§7● §eClick to change to page " + page + ".");
        } else {
            nextIcon.setMaterial(Material.GUNPOWDER);
            nextIcon.addLoreLine("§7● §eThere's no next page.");
        }

        Item pageIcon = new Item(Material.PAPER)
                .setName("§2" + page + ". §aPage");


        inv.setItem(ChestSlot.getSlot((invsize / 9), 8), previousIcon.getItemStack());
        inv.setItem(ChestSlot.getSlot((invsize / 9), 9), nextIcon.getItemStack());

        inv.setItem(ChestSlot.getSlot((invsize / 9), 5), pageIcon.getItemStack());

        inv.setItem(ChestSlot.getSlot((invsize / 9), 1), getBack().getItemStack());


        List<Player> playerselection = (List<Player>) InventoryHelper.getPageContent(players, itemsPerPage, page);

        Item head = new Item(Material.PLAYER_HEAD);

        for (Player p : playerselection) {
            head.setSkullOwner(p.getName());
            head.setName("§a" + p.getName());
            head.addLoreLine("§7● §eClick to select " + p.getName());
            head.addLoreLine("");
            head.addLoreLine("§7● §eLifes: §6" + p.getHealth() + "/" + p.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
            head.addLoreLine("§7● §eSaturation: §6" + p.getFoodLevel() + "/20");

            inv.addItem(head.getItemStack());
        }


        player.openInventory(inv);
    }

    public static String getToggleableModuleMenuTitle() {
        return "§bToggleable Modules";
    }

    public static void openToggleableModuleMenu(Player player, Integer page) {
        int invsize = 9 * 5;
        Inventory inv = Bukkit.createInventory(null, invsize, getToggleableModuleMenuTitle());
        int itemsPerPage = invsize - (9 * 2);
        List<TrollModule> modules = new ArrayList<>();
        for (TrollModule module : TrollModuleManager.getModules()) {
            if (module instanceof ToggleableTrollModule) {
                modules.add(module);
            }
        }

        Item placeholder = new Item(Material.BLACK_STAINED_GLASS_PANE)
                .setName("§6");

        for (int i = ChestSlot.getSlot(4, 1); i <= ChestSlot.getSlot(4, 9); i++) {
            inv.setItem(i, placeholder.getItemStack());
        }

        boolean previous = (page > 1);
        boolean next = modules.size() >= (itemsPerPage * page);

        Item previousIcon = new Item(Material.STONE)
                .setName(getPreviousPageItemName());

        if (previous) {
            previousIcon.setMaterial(Material.SUGAR);
            previousIcon.addLoreLine("§7● §eClick to change to page " + page + ".");
        } else {
            previousIcon.setMaterial(Material.GUNPOWDER);
            previousIcon.addLoreLine("§7● §eThere's no previous page.");
        }

        Item nextIcon = new Item(Material.STONE)
                .setName(getNextPageItemName());

        if (next) {
            nextIcon.setMaterial(Material.SUGAR);
            nextIcon.addLoreLine("§7● §eClick to change to page " + page + ".");
        } else {
            nextIcon.setMaterial(Material.GUNPOWDER);
            nextIcon.addLoreLine("§7● §eThere's no next page.");
        }

        Item pageIcon = new Item(Material.PAPER)
                .setName("§2" + page + ". §aPage");

        inv.setItem(ChestSlot.getSlot((invsize / 9), 1), getBack().getItemStack());

        inv.setItem(ChestSlot.getSlot((invsize / 9), 8), previousIcon.getItemStack());
        inv.setItem(ChestSlot.getSlot((invsize / 9), 9), nextIcon.getItemStack());

        inv.setItem(ChestSlot.getSlot((invsize / 9), 5), pageIcon.getItemStack());

        for (TrollModule module : modules) {
            Item moduleIcon = new Item(module.getIcon())
                    .setName("§a" + module.getName());

            moduleIcon.addLoreLine("§7● §fActive: " + (((ToggleableTrollModule) module).getActive().contains(getTrolling(player))));

            moduleIcon.addLoreLine("§7● §fPermission: §e" + TrollModuleManager.getPermission(module) + " §7(" + (player.hasPermission(TrollModuleManager.getPermission(module)) ? "§a✔" : "§c✘") + "§7)");

            boolean addCaption = true;

            for (String description : module.getDescription()) {
                moduleIcon.addLoreLine((addCaption ? "§7● §fDescription: §e" : "§e") + description);

                if (addCaption) {
                    addCaption = false;
                }
            }
            inv.addItem(moduleIcon.getItemStack());
        }
        player.openInventory(inv);
    }

    public static String getOnetimeModuleMenuTitle() {
        return "§bOnetime Modules";
    }

    public static void openOnetimeModuleMenu(Player player, Integer page) {
        int invsize = 9 * 5;
        Inventory inv = Bukkit.createInventory(null, invsize, getToggleableModuleMenuTitle());
        int itemsPerPage = invsize - (9 * 2);
        List<TrollModule> modules = new ArrayList<>();
        for (TrollModule module : TrollModuleManager.getModules()) {
            if (module instanceof OneTimeTrollModule) {
                modules.add(module);
            }
        }

        Item placeholder = new Item(Material.BLACK_STAINED_GLASS_PANE)
                .setName("§6");

        for (int i = ChestSlot.getSlot(4, 1); i <= ChestSlot.getSlot(4, 9); i++) {
            inv.setItem(i, placeholder.getItemStack());
        }

        boolean previous = (page > 1);
        boolean next = modules.size() >= (itemsPerPage * page);

        Item previousIcon = new Item(Material.STONE)
                .setName(getPreviousPageItemName());

        if (previous) {
            previousIcon.setMaterial(Material.SUGAR);
            previousIcon.addLoreLine("§7● §eClick to change to page " + page + ".");
        } else {
            previousIcon.setMaterial(Material.GUNPOWDER);
            previousIcon.addLoreLine("§7● §eThere's no previous page.");
        }

        Item nextIcon = new Item(Material.STONE)
                .setName(getNextPageItemName());

        if (next) {
            nextIcon.setMaterial(Material.SUGAR);
            nextIcon.addLoreLine("§7● §eClick to change to page " + page + ".");
        } else {
            nextIcon.setMaterial(Material.GUNPOWDER);
            nextIcon.addLoreLine("§7● §eThere's no next page.");
        }

        Item pageIcon = new Item(Material.PAPER)
                .setName("§2" + page + ". §aPage");

        inv.setItem(ChestSlot.getSlot((invsize / 9), 1), getBack().getItemStack());

        inv.setItem(ChestSlot.getSlot((invsize / 9), 8), previousIcon.getItemStack());
        inv.setItem(ChestSlot.getSlot((invsize / 9), 9), nextIcon.getItemStack());

        inv.setItem(ChestSlot.getSlot((invsize / 9), 5), pageIcon.getItemStack());

        for (TrollModule module : modules) {
            Item moduleIcon = new Item(module.getIcon())
                    .setName("§a" + module.getName());

            moduleIcon.addLoreLine("§7● §fPermission: §e" + TrollModuleManager.getPermission(module) + " §7(" + (player.hasPermission(TrollModuleManager.getPermission(module)) ? "§a✔" : "§c✘") + "§7)");

            boolean addCaption = true;

            for (String description : module.getDescription()) {
                moduleIcon.addLoreLine((addCaption ? "§7● §fDescription: §e" : "§e") + description);

                if (addCaption) {
                    addCaption = false;
                }
            }
            inv.addItem(moduleIcon.getItemStack());
        }
        player.openInventory(inv);
    }

}
