package de.simplyproductions.trollsystem.utils;

public enum ServerMode {

    PERFORMANCE,
    STANDARD;

}
