package de.simplyproductions.trollsystem.utils;

import org.bukkit.entity.Player;

public abstract class OneTimeTrollModule implements TrollModule {

    public abstract boolean trigger(Player executor);

}
