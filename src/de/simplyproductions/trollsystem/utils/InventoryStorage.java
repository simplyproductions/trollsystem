package de.simplyproductions.trollsystem.utils;

import de.chatvergehen.spigotapi.util.filemanaging.ConfigEditor;
import de.chatvergehen.spigotapi.util.filemanaging.FileBuilder;
import de.simplyproductions.trollsystem.main.Main;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.List;

public class InventoryStorage {

    public static void save(Player p, boolean clear) throws IOException {
        FileBuilder fb = new FileBuilder(Main.getInstance().getDataFolder().getPath() + "//Data//Players//" + p.getUniqueId().toString(), "inventory.yml");
        fb.create();
        ConfigEditor ce = fb.getConfig();

        ce.set("Inventory.Armor", p.getInventory().getArmorContents());
        ce.set("Inventory.Content", p.getInventory().getContents());

        if (clear) {
            p.getInventory().clear();

            p.getInventory().setHelmet(new ItemStack(Material.AIR));
            p.getInventory().setChestplate(new ItemStack(Material.AIR));
            p.getInventory().setLeggings(new ItemStack(Material.AIR));
            p.getInventory().setBoots(new ItemStack(Material.AIR));
        }
    }

    @SuppressWarnings("unchecked")
    public static boolean restore(Player p, boolean clear) throws IOException {
        FileBuilder fb = new FileBuilder(Main.getInstance().getDataFolder().getPath() + "//Data//Players//" + p.getUniqueId().toString(), "inventory.yml");
        if (fb.exists()) {
            if (clear) {
                p.getInventory().clear();

                p.getInventory().setHelmet(new ItemStack(Material.AIR));
                p.getInventory().setChestplate(new ItemStack(Material.AIR));
                p.getInventory().setLeggings(new ItemStack(Material.AIR));
                p.getInventory().setBoots(new ItemStack(Material.AIR));
            }

            ConfigEditor ce = fb.getConfig();


            ItemStack[] content = ((List<ItemStack>) ce.get("Inventory.Armor")).toArray(new ItemStack[0]);
            p.getInventory().setArmorContents(content);
            content = ((List<ItemStack>) ce.get("Inventory.Content")).toArray(new ItemStack[0]);
            p.getInventory().setContents(content);

            fb.delete();
            return true;
        }
        return false;
    }

}
