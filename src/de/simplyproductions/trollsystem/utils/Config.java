package de.simplyproductions.trollsystem.utils;

import de.chatvergehen.spigotapi.util.filemanaging.ConfigEditor;
import de.chatvergehen.spigotapi.util.filemanaging.FileBuilder;
import de.chatvergehen.spigotapi.util.filemanaging.FolderBuilder;
import de.simplyproductions.trollsystem.main.Main;

public class Config {

    public static FileBuilder getFile() {
        return new FileBuilder(Main.getInstance().getDataFolder().getAbsolutePath(), "config.yml");
    }

    public static ConfigEditor getConfig() {
        return getFile().getConfig();
    }

    public enum Configuration {

        SERVERMODE("ServerMode", ServerMode.PERFORMANCE.name());

        private String path;
        private Object standardValue;

        Configuration(String path, Object standardValue) {
            this.path = path;
            this.standardValue = standardValue;
        }

        public static void setupConfig() {
            if (!getFile().exists()) {
                getFile().create();
            }

            for (Configuration cfg : values()) {
                if (!getConfig().contains(cfg.path)) {
                    getConfig().set(cfg.path, cfg.standardValue);
                }
            }
        }

        public static Object get(Configuration configuration) {
            return getConfig().get(configuration.getPath());
        }

        public static void set(Configuration configuration, Object value) {
            getConfig().set(configuration.getPath(), value);
        }

        public String getPath() {
            return path;
        }

        public Object getStandardValue() {
            return standardValue;
        }

    }

}
