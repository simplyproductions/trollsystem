package de.simplyproductions.trollsystem.listeners.custom;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class callCustomEvents implements Listener {

    @EventHandler
    public void moveEvent(PlayerMoveEvent e) {
        Player p = e.getPlayer();

        if (e.getFrom().getBlockX() != e.getTo().getBlockX() || e.getFrom().getBlockY() != e.getTo().getBlockY() || e.getFrom().getBlockZ() != e.getTo().getBlockZ()) {
            PlayerWalkEvent event = new PlayerWalkEvent(p, e.getFrom(), e.getTo());
            Bukkit.getPluginManager().callEvent(event);

            if(event.isCancelled()) {
                e.setCancelled(true);
            }
        }
    }

}
