package de.simplyproductions.trollsystem.listeners;

import de.simplyproductions.trollsystem.utils.*;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class Listeners_Trollmenu implements Listener {

    ItemList list = new ItemList();

    @EventHandler
    public void use(PlayerInteractEvent event) {
        Player p = event.getPlayer();

        if (p.getInventory().getItemInMainHand().isSimilar(list.getTroll().getItemStack())) {
            if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                event.setCancelled(true);

                TrollModeManager.openTrollMenu(p);
            }
        }
    }

    @EventHandler
    public void antidrop(PlayerDropItemEvent event) {
        if (event.getItemDrop().getItemStack().isSimilar(list.getTroll().getItemStack())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void antiinv(InventoryClickEvent event) {
        if (event.getView().getTitle().equalsIgnoreCase(TrollModeManager.getTrollMenuTitle())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void mainMenu(InventoryClickEvent event) {
        Player p = (Player) event.getWhoClicked();

        if (event.getView().getTitle().equalsIgnoreCase(TrollModeManager.getTrollMenuTitle())) {
            event.setCancelled(true);

            if (event.getCurrentItem() != null) {
                if (event.getCurrentItem().getType() == Material.PLAYER_HEAD) {
                    TrollModeManager.openPlayerSelection(p, 1);
                    p.playSound(p.getLocation(), Sound.BLOCK_CHEST_OPEN, 2, 1);
                } else if (event.getCurrentItem().isSimilar(TrollModeManager.getToggleableModuleIcon().getItemStack())) {
                    TrollModeManager.openToggleableModuleMenu(p, 1);
                    p.playSound(p.getLocation(), Sound.BLOCK_CHEST_OPEN, 2, 1);
                } else if (event.getCurrentItem().isSimilar(TrollModeManager.getOneTimeModuleIcon().getItemStack())) {
                    TrollModeManager.openOnetimeModuleMenu(p, 1);
                    p.playSound(p.getLocation(), Sound.BLOCK_CHEST_OPEN, 2, 1);
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void playerSelection(InventoryClickEvent event) {
        Player p = (Player) event.getWhoClicked();

        if (event.getView().getTitle().equalsIgnoreCase(TrollModeManager.getPlayerSelectionMenuTitle())) {
            event.setCancelled(true);

            if (event.getCurrentItem() != null) {
                if (event.getCurrentItem().getType() == Material.PLAYER_HEAD) {
                    OfflinePlayer target = Bukkit.getOfflinePlayer(ChatColor.stripColor(new Item(event.getCurrentItem()).getName()));

                    // TODO: Page turn

                    if (target.isOnline()) {
                        p.sendMessage("§7You're trolling §b" + target.getName() + " §7now.");

                        p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_USE, 2, 1);

                        TrollModeManager.setTrolling(p, target);
                        TrollModeManager.openTrollMenu(p);
                    } else {
                        p.closeInventory();

                        p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 2, 1);

                        p.sendMessage("§c" + target.getName() + " isn't online at the moment!");
                    }
                } else if (event.getCurrentItem().isSimilar(TrollModeManager.getBack().getItemStack())) {
                    TrollModeManager.openTrollMenu(p);
                }
            }
        }
    }

    @EventHandler
    public void toggleableModuleUse(InventoryClickEvent event) {
        Player p = (Player) event.getWhoClicked();

        if (event.getView().getTitle().equalsIgnoreCase(TrollModeManager.getToggleableModuleMenuTitle())) {
            OfflinePlayer target = TrollModeManager.getTrolling(p);
            event.setCancelled(true);

            if (event.getCurrentItem() != null) {
                if (!event.getCurrentItem().isSimilar(TrollModeManager.getBack().getItemStack())) {
                    ToggleableTrollModule module = (ToggleableTrollModule) TrollModuleManager.getByName(new Item(event.getCurrentItem()).getName());

                    if (module != null) {
                        if (p.hasPermission(TrollModuleManager.getPermission(module))) {
                            boolean active = module.toggle(p);

                            p.sendMessage("§7TrollModule §b" + module.getName() + " " + (active ? "§aactivated" : "§cdeactivated") + " §7for §e" + target.getName() + "§7.");

                            if (active) {
                                p.playSound(p.getLocation(), Sound.BLOCK_PISTON_EXTEND, 2, 1);
                            } else {
                                p.playSound(p.getLocation(), Sound.BLOCK_PISTON_CONTRACT, 2, 1);
                            }
                        } else {
                            p.closeInventory();

                            p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 2, 1);
                            p.sendMessage("§cYou lack the permission §e" + TrollModuleManager.getPermission(module));
                        }
                    }
                } else {
                    TrollModeManager.openTrollMenu(p);
                }
            }
        }
    }

    @EventHandler
    public void onetimeModuleUse(InventoryClickEvent event) {
        Player p = (Player) event.getWhoClicked();

        if (event.getView().getTitle().equalsIgnoreCase(TrollModeManager.getToggleableModuleMenuTitle())) {
            OfflinePlayer target = TrollModeManager.getTrolling(p);
            event.setCancelled(true);

            if (event.getCurrentItem() != null) {
                if (!event.getCurrentItem().isSimilar(TrollModeManager.getBack().getItemStack())) {
                    OneTimeTrollModule module = (OneTimeTrollModule) TrollModuleManager.getByName(new Item(event.getCurrentItem()).getName());

                    if (module != null) {
                        if (p.hasPermission(TrollModuleManager.getPermission(module))) {
                            boolean success = module.trigger(p);

                            if (success) {
                                p.sendMessage("§7TrollModule §b" + module.getName() + " §7was §aexecuted §7for §e" + target.getName() + "§7.");
                                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 2, 1);
                            } else {
                                p.sendMessage("§7TrollModule §b" + module.getName() + " §7couldn't be executed §7for §e" + target.getName() + "§7.");
                            }
                        } else {
                            p.closeInventory();

                            p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 2, 1);
                            p.sendMessage("§cYou lack the permission §e" + TrollModuleManager.getPermission(module));
                        }
                    }
                } else {
                    TrollModeManager.openTrollMenu(p);
                }
            }
        }
    }

}
