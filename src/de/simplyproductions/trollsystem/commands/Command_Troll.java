package de.simplyproductions.trollsystem.commands;

import de.simplyproductions.trollsystem.main.Main;
import de.simplyproductions.trollsystem.utils.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import java.io.IOException;
import java.util.List;


public class Command_Troll implements CommandExecutor {

    ItemList list = new ItemList();

    @Override
    public boolean onCommand(CommandSender cs, Command command, String l, String[] args) {
        List<TrollModule> modules = TrollModuleManager.getModules();

        if(cs.hasPermission("Troll.Command.Troll")) {
            if (args.length == 0) {
                if (cs instanceof Player) {
                    Player p = ((Player) cs).getPlayer();

                    if (!TrollModeManager.getActive(p)) {
                        try {
                            InventoryStorage.save(p, true);
                            TrollModeManager.giveItems(p, false);
                            TrollModeManager.setActive(p, true);

                            p.sendMessage("§7You've §aentered §7the §eTrollmode§7.");
                        } catch (IOException e) {
                            p.sendMessage("§cThe Trollmode couldn't be entered.");
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            InventoryStorage.restore(p, true);
                            TrollModeManager.setActive(p, false);

                            p.sendMessage("§7You've §cleft §7the §eTrollmode§7.");
                        } catch (IOException e) {
                            e.printStackTrace();
                            p.sendMessage("§cThe Trollmode couldn't be left.");
                        }
                    }
                }
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("info")) {
                    PluginDescriptionFile pdf = Main.getInstance().getDescription();
                    StringBuilder authors = new StringBuilder();

                    for (String author : pdf.getAuthors()) {
                        authors.append(author + ", ");
                    }
                    authors.delete(authors.length() - 2, authors.length());


                    cs.sendMessage("§b" + pdf.getName() + " v" + pdf.getVersion() + " §7by §b" + authors.toString());
                    cs.sendMessage("§7" + (modules.size() == 1 ? "There is §b1" : "There are §b" + modules.size()) + " loaded Module" + (modules.size() != 1 ? "s" : "") + ".");
                    cs.sendMessage("§7List all Modules using §3/" + l + " info modules");
                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("info")) {
                    if (args[1].equalsIgnoreCase("modules")) {
                        for (TrollModule module : modules) {
                            cs.sendMessage("§7- §b" + module.getName());
                        }

                        cs.sendMessage("");
                        cs.sendMessage("§7More information about a module: §3/" + l + " info module <Name>");
                    }
                } else if (args[0].equalsIgnoreCase("debug")) {
                    if (cs instanceof Player) {
                        Player p = (Player) cs;

                        if (args[1].equalsIgnoreCase("saveinventory")) {
                            try {
                                InventoryStorage.save(p, true);
                                p.sendMessage("§aDein Inventar wurde gespeichert.");
                            } catch (IOException e) {
                                e.printStackTrace();
                                p.sendMessage("§aDein Inventar konnte nicht gespeichert werden.");
                            }
                        } else if (args[1].equalsIgnoreCase("loadinventory")) {
                            try {
                                boolean success = InventoryStorage.restore(p, true);

                                if (success) {
                                    p.sendMessage("§aDein Inventar wurde wiederhergestellt.");
                                } else {
                                    p.sendMessage("§cEs wurde kein gespeichertes Inventar gefunden!");
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                p.sendMessage("§aDein Inventar konnte nicht wiederhergestellt werden.");
                            }
                        }
                    }
                }
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("info")) {
                    if (args[1].equalsIgnoreCase("module")) {
                        TrollModule module = TrollModuleManager.getByName(args[2]);

                        if (module != null) {
                            cs.sendMessage("§7Information about the §b" + module.getName() + " Module§7:");
                            cs.sendMessage("");
                            cs.sendMessage("§fNeeded Permission: §e" + TrollModuleManager.getPermission(module) + " §7(" + (cs.hasPermission(TrollModuleManager.getPermission(module)) ? "§a✔" : "§c✘") + "§7)");
                            cs.sendMessage("§fToggleable: " + (module instanceof ToggleableTrollModule ? "§a✔" : "§c✘"));
                        } else {
                            cs.sendMessage("§cThere's no Module matching your search!");
                        }
                    }
                }
            }
        }else {
            cs.sendMessage("§cYou aren't allowed to do that!");
        }
        return true;
    }

}
