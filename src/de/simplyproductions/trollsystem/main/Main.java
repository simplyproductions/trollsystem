package de.simplyproductions.trollsystem.main;

import de.simplyproductions.trollsystem.commands.Command_Troll;
import de.simplyproductions.trollsystem.listeners.Listeners_Trollmenu;
import de.simplyproductions.trollsystem.listeners.custom.callCustomEvents;
import de.simplyproductions.trollsystem.utils.Config;
import de.simplyproductions.trollsystem.utils.TrollModuleManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static Plugin plugin;

    @Override
    public void onEnable() {
        if (!(Bukkit.getPluginManager().getPlugin("SpigotAPI") != null && Bukkit.getPluginManager().isPluginEnabled("SpigotAPI"))) {
            System.err.println("TrollSystem couldn't be loaded! You didn't install the SpigotAPI.jar Plugin.\nBut this is an API and have to be installed to for this system to work.");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        plugin = this;

        Config.Configuration.setupConfig();

        registerCommands();
        registerEvents();

        TrollModuleManager.registerModules();
    }

    @Override
    public void onDisable() {

    }

    public static Plugin getInstance() {
        return plugin;
    }

    private void registerCommands() {
        getCommand("troll").setExecutor(new Command_Troll());
    }

    private void registerEvents() {
        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new callCustomEvents(), this);
        pm.registerEvents(new Listeners_Trollmenu(), this);
    }

}
